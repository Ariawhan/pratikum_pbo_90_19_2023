// Nama : I Gede Ariawan Eka Putra
// Nim  : 1908561085

// Tugas Nomor 2 
// Buatlah sebuah program untuk menampilkan string yang berisikan biodata diri masingNama, nm, alamat, no telp.

package Tugas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 *
 * @author Ariawan
 */

public class nomor_2 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Tugas No 2");
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6, 2, 10, 10));

        // Input Nama
        JLabel labelNama = new JLabel("Nama: ");
        JTextField textFieldNama = new JTextField(20);

        // Input NIM
        JLabel labelNim = new JLabel("NIM: ");
        JTextField textFieldNim = new JTextField(20);

        // Input Alamat
        JLabel labelAlamat = new JLabel("Alamat: ");
        JTextField textFieldAlamat = new JTextField(20);

        // Input No Hp
        JLabel labelNoTelp = new JLabel("No Telp: ");
        JTextField textFieldNoTelp = new JTextField(20);

        panel.add(labelNama);
        panel.add(textFieldNama);
        panel.add(labelNim);
        panel.add(textFieldNim);
        panel.add(labelAlamat);
        panel.add(textFieldAlamat);
        panel.add(labelNoTelp);
        panel.add(textFieldNoTelp);

        // Tombol View
        JButton buttonSimpan = new JButton("View");
        panel.add(buttonSimpan);

        buttonSimpan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                String nama = textFieldNama.getText();
                String nim = textFieldNim.getText();
                String alamat = textFieldAlamat.getText();
                String noTelp = textFieldNoTelp.getText();

                if (nama.isEmpty() || nim.isEmpty() || alamat.isEmpty() || noTelp.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Lengkapi biodata Anda!", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    String message = "Biodata Diri:\nNama: " + nama + "\nNIM: " + nim + "\nAlamat: " + alamat + "\nNo Telp: " + noTelp;
                    JOptionPane.showMessageDialog(null, message, "Data Diri", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        frame.add(panel);
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
